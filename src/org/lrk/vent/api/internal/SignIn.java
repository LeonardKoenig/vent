package org.lrk.vent.api.internal;

import org.lrk.vent.api.internal.Emotion;
import org.lrk.vent.api.internal.EmotionCategory;
import org.lrk.vent.api.internal.InviteTier;
import org.lrk.vent.api.internal.User;

public class SignIn {
	public User user;
	InviteTier[] inviteTiers;
	Emotion[] emotions;
	EmotionCategory[] emotionCategories;
}
