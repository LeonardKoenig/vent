package org.lrk.vent.api.internal;

import java.util.UUID;

public class Emotion {
	UUID id;
	String name;
	UUID emotionCategoryId;
	UUID[] enabledInteractions;
	Integer position;
	Boolean visible;
}
