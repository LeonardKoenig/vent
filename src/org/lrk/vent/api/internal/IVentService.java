package org.lrk.vent.api.internal;

import org.lrk.vent.api.internal.wrap.*;
import retrofit2.Call;
import retrofit2.http.*;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Map;
import java.util.UUID;

public interface IVentService {

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@POST("api/v1/sign_in.json")
	Call<SignIn> signIn(@Body Map<String, Map<String, String>> loginInformation);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@GET("api/v1/my/feed.json")
	Call<WrapVents> getFeed(@Header("X-User-Username") String username,
	                        @Header("X-User-Token") String authenticationToken,
	                        @Header("X-3PO") String deviceId1,
	                        @Header("X-OB1") String devicedId2,
	                        @Header("X-User-Platform") String platform,
	                        @Query("per_page") int perPage, @Query("from[order]") String from_order,
	                        @Query("from[field]") String from_field, @Query("from[value]") String from_value);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@POST("api/v1/my/vents.json")
	Call<WrapVent> postVent(@Header("X-User-Username") String username,
	                        @Header("X-User-Token") String authenticationToken,
	                        @Header("X-3PO") String deviceId1,
	                        @Header("X-OB1") String devicedId2,
	                        @Header("X-User-Platform") String platform,
	                        @Body Vent vent);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@GET("api/v1/users/{user}")
	Call<WrapUser> getUser(@Header("X-User-Username") String username,
	                       @Header("X-User-Token") String authenticationToken,
	                       @Header("X-3PO") String deviceId1,
	                       @Header("X-OB1") String devicedId2,
	                       @Header("X-User-Platform") String platform,
	                       @Path("user") String userId);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@GET("api/v1/emotion_categories.json")
	Call<WrapEmotionCategories> getEmotionCategories(@Header("X-User-Username") String username,
	                                                 @Header("X-User-Token") String authenticationToken,
	                                                 @Header("X-3PO") String deviceId1,
	                                                 @Header("X-OB1") String devicedId2,
	                                                 @Header("X-User-Platform") String platform);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@GET("api/v1/my/notification_count.json")
	Call<NotificationCount> getNotificationCount(@Header("X-User-Username") String username,
	                    @Header("X-User-Token") String authenticationToken,
	                    @Header("X-3PO") String deviceId1,
	                    @Header("X-OB1") String devicedId2,
	                    @Header("X-User-Platform") String platform);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@GET("api/v1/my/notifications.json")
	Call<WrapNotifications> getNotifications(@Header("X-User-Username") String username,
	                                         @Header("X-User-Token") String authenticationToken,
	                                         @Header("X-3PO") String deviceId1,
	                                         @Header("X-OB1") String devicedId2,
	                                         @Header("X-User-Platform") String platform,
	                                         @Query("per_page") int perPage,
	                                         @Query("from[order]") String order, // desc
	                                         @Query("from[field]") String field, // last_active_at
	                                         @Query("from[value]") String value); // Timestamp MAX


	//TODO Body: {} ??
	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@PUT("api/v1/my/notifications/mark_all")
	Call<WrapNotifications> markAllNotifications(@Header("X-User-Username") String username,
	                                             @Header("X-User-Token") String authenticationToken,
	                                             @Header("X-3PO") String deviceId1,
	                                             @Header("X-OB1") String devicedId2,
	                                             @Header("X-User-Platform") String platform);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@GET("api/v1/users/autocomplete.json")
	Call<WrapUsers> usersAutocomplete(@Header("X-User-Username") String username,
	                                  @Header("X-User-Token") String authenticationToken,
	                                  @Header("X-3PO") String deviceId1,
	                                  @Header("X-OB1") String devicedId2,
	                                  @Header("X-User-Platform") String platform,
	                                  @Query("per_page") int perPage,
	                                  @Query("q") String query);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@GET("api/v1/vents/{vent}.json")
	Call<WrapVent> getVent(@Header("X-User-Username") String username,
	                       @Header("X-User-Token") String authenticationToken,
	                       @Header("X-3PO") String deviceId1,
	                       @Header("X-OB1") String devicedId2,
	                       @Header("X-User-Platform") String platform,
	                       @Path("vent") UUID ventId);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@GET("api/v1/vents/{vent}/comments.json")
	Call<WrapComments> getVentComments(@Header("X-User-Username") String username,
	                                   @Header("X-User-Token") String authenticationToken,
	                                   @Header("X-3PO") String deviceId1,
	                                   @Header("X-OB1") String devicedId2,
	                                   @Header("X-User-Platform") String platform,
	                                   @Path("vent") UUID ventId,
	                                   @Query("per_page") int perPage,
	                                   @Query("from[order]") String order,
	                                   @Query("from[field]") String field,
	                                   @Query("from[value]") Timestamp createdAt);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@DELETE("api/v1/vents/{vent}.json")
	Call<Void> deleteVent(@Header("X-User-Username") String username,
	                      @Header("X-User-Token") String authenticationToken,
	                      @Header("X-3PO") String deviceId1,
	                      @Header("X-OB1") String devicedId2,
	                      @Header("X-User-Platform") String platform,
	                      @Path("vent") UUID ventId);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@GET("api/v1/vents/most_discussed.json")
	Call<WrapVents> getMostDiscussed(@Header("X-User-Username") String username,
	                                 @Header("X-User-Token") String authenticationToken,
	                                 @Header("X-3PO") String deviceId1,
	                                 @Header("X-OB1") String devicedId2,
	                                 @Header("X-User-Platform") String platform,
	                                 @Query("per_page") int perPage);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@GET("api/v1/search_suggestions.json")
	Call<WrapVents> getSearchSuggestions(@Header("X-User-Username") String username,
	                                     @Header("X-User-Token") String authenticationToken,
	                                     @Header("X-3PO") String deviceId1,
	                                     @Header("X-OB1") String devicedId2,
	                                     @Header("X-User-Platform") String platform,
	                                     @Query("per_page") int perPage);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@GET("api/v1/search_suggestions.json")
	Call<WrapSearchSuggestions> getSearchSuggestions(@Header("X-User-Username") String username,
	                                                 @Header("X-User-Token") String authenticationToken,
	                                                 @Header("X-3PO") String deviceId1,
	                                                 @Header("X-OB1") String devicedId2,
	                                                 @Header("X-User-Platform") String platform,
	                                                 @Query("per_page") int perPage,
	                                                 @Query("q[value_start]") String query);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@GET("api/v1/users/search.json")
	Call<WrapUsers> searchUser(@Header("X-User-Username") String username,
	                          @Header("X-User-Token") String authenticationToken,
	                          @Header("X-3PO") String deviceId1,
	                          @Header("X-OB1") String devicedId2,
	                          @Header("X-User-Platform") String platform,
	                          @Query("per_page") int perPage,
	                          @Query("q") String query);

	@Headers({"Content-Type: application/json", "charset: utf-8"})
	@GET("api/v1/vents.json")
	Call<WrapVent> getUserVents(@Header("X-User-Username") String username,
	                            @Header("X-User-Token") String authenticationToken,
	                            @Header("X-3PO") String deviceId1,
	                            @Header("X-OB1") String devicedId2,
	                            @Header("X-User-Platform") String platform,
	                            @Query("q[user_id_eq]") UUID userId,
	                            @Query("per_page") int perPage,
	                            @Query("from[order]") String order,
	                            @Query("from[field") String createdAt,
	                            @Query("from[value]") Timestamp timestamp);


}
