package org.lrk.vent.api.internal.wrap;

import org.lrk.vent.api.internal.Comment;

public class WrapComments {
	Comment[] comments;
	Meta meta;

	public static class Meta {
		String deletedIds;
		Boolean moreAvailable;
	}
}
