package org.lrk.vent.api.internal;

import java.awt.*;
import java.util.UUID;

public class EmotionCategory {
	UUID id;
	String name;
	String color;
	String fontColor;
	Integer position;
	Boolean visible;
	Boolean purchasable;
	String androidSKU;
	String iOSSKU;
	UUID[] emotionIds;
}
