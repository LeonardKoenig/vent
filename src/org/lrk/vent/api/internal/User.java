package org.lrk.vent.api.internal;

import java.net.URL;
import java.sql.Timestamp;
import java.util.Map;
import java.util.UUID;

public class User {
	Boolean blockedByYou;
	Boolean isBlockingYou;
	Boolean youListenTou;
	Boolean youRequestedListenTo;
	Boolean requestedListenToYou;
	Boolean youSubscribedTo;
	String placeholder;
	URL profileImageURL;
	Map<String, URL> profileImageVersions;
	UUID id;
	public String username;
	String name; //TODO is null...?!?
	String bio;
	public Timestamp createdAt;
	Timestamp updatedAt;
	UUID currentEmotionId;
	Map<String, String>[] mentionedUsers;
	Map<String, String>[] links;
	Integer ventCount;
	Boolean accountIsPublic;
	Integer listeningCount;
	Integer listenerCount;
	Boolean confirmedEmail;
	Boolean messagesEnabled;
	String email;
	Boolean typingIndicatorsEnabled;
	Boolean readReceiptsEnabled;
	Boolean hasPrivateBio;
	Boolean nsfwSetting;
	Boolean adsEnabled;
	public String authenticationToken;
	Timestamp authMigratedAt;
	Timestamp firstVentedAt;
	UUID inviteTierId;
	UUID currentEmotionCategoryId;
	UUID groupID; //TODO is null...?!?
}
