package org.lrk.vent.api.internal.wrap;

import org.lrk.vent.api.internal.EmotionCategory;

import java.util.UUID;

public class WrapEmotionCategories {
	public EmotionCategory[] emotionCategories;
	public Meta meta;

	public static class Meta {
		UUID[] deletedIds;
	}
}
